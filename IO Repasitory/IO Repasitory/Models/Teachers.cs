﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO_Repasitory.Models
{
    class Teachers
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }
        public Region Region { get; set; }

        public string Information => $"{Name} {Surename} {Age} {University} {Region}";

        public override string ToString() => Information;
    }
}
