﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO_Repasitory.Models;
using IO_Repasitory.Repasitorys_Model;

namespace IO_Repasitory
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = CreatStudents(5).ToList(); ;
            var list = new Students_Repasitory();
            list.AddRange(student);
            list.SaveChanges();
            IEnumerable<Students> students = list.AsEnumerable();
            List<Students> studen = students.ToList();
            foreach (var item in studen)
            {
                Console.WriteLine(item);
            }
            
        }

        static IEnumerable<Students> CreatStudents(int count)
        {
            string[] name =
            {
                "Artak",
                "Anahit",
                "Meri",
                "Arman",
                "Lusine"
            };

            string[] surename =
            {
                "Hovanisyan",
                "Amirxanyan",
                "Mheryan",
                "Simonyan",
                "Melqonyan"
            };

            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var index = rnd.Next(0, name.Length);
                var index2 = rnd.Next(0, surename.Length);
                yield return new Students
                {
                    Name = name[index],
                    Surename = surename[index],
                    Age = (byte)rnd.Next(18, 45),
                    University = (University)rnd.Next(0, 6),
                    Region = (Region)rnd.Next(0, 12),
                    School = (School)rnd.Next(0, 3)
                };
            }
        }
    }
}
