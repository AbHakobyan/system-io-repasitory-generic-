﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO_Repasitory.Repasitorys_Model
{
    interface IBaseRepasitory<T> where T : class,new()
    {
        int SaveChanges();
        IEnumerable<T> AsEnumerable();
        void Add(T model);
        void Insert(int index,T model);
        void AddRange(IEnumerable<T> model);
    }
}
