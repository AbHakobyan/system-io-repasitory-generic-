﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO_Repasitory;

namespace IO_Repasitory.Models
{
    class Students
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        public byte Age { get; set; }
        public University University { get; set; }
        public School School { get; set; }
        public Region Region { get; set; }

        public string Information => $"{Name} {Surename} {Age} {University} {Region} {School} ";

        public override string ToString() => Information;
        
    }
}
