﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO_Repasitory
{
    public enum University : byte
    {
        YSU,
        AUA,
        ASPU,
        RASU,
        YSMU,
        ANAU
    }

    public enum School : byte
    {
        High,
        Midle,
        Easy,
    }

    public enum Region : byte
    {
        Aragatsotn,
        Ararat,
        Armavir,
        Gegharkunik,
        Kotayk,
        Lori,
        Shirak,
        Syunik,
        Tavush,
        VayotsDzor,
        Yerevan
    }
}
