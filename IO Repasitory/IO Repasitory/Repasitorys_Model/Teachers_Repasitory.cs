﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO_Repasitory.Models;

namespace IO_Repasitory.Repasitorys_Model
{
    class Teachers_Repasitory : Base_Repasitory<Teachers>
    {
        public override string Path => "teachers.txt";

        public override void OnReadFill(string[] data, Teachers model)
        {
            switch (data[0])
            {
                case nameof(model.Name):
                    model.Name = data[1];
                    break;
                case nameof(model.Surename):
                    model.Surename = data[1];
                    break;
                case nameof(model.Age):
                    if (byte.TryParse(data[1], out byte age))
                        model.Age = age;
                    break;
                case nameof(model.University):
                    if (byte.TryParse(data[1], out byte university))
                        model.University = (University)university;
                    break;
                case nameof(model.Region):
                    if (byte.TryParse(data[1], out byte region))
                        model.Region = (Region)region;
                    break;
            }
        }

        public override void WriteModel(StreamWriter writer, Teachers models)
        {
            writer.WriteLine($"{nameof(models.Name)}:{models.Name}");
            writer.WriteLine($"{nameof(models.Surename)}:{models.Surename}");
            writer.WriteLine($"{nameof(models.Age)}:{models.Age}");
            writer.WriteLine($"{nameof(models.University)}:{models.University}");
            writer.WriteLine($"{nameof(models.Region)}:{models.Region}");
        }
    }
}
