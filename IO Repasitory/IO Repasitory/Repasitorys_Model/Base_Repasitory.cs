﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using IO_Repasitory.Repasitorys_Model;
using IO_Repasitory.Models;


namespace IO_Repasitory.Repasitorys_Model 
{
    abstract class Base_Repasitory<T> : IBaseRepasitory<T> where T : class,new() 
    {
        public Base_Repasitory()
        {
            _list = AsEnumerable().ToList();
        }
        private List<T> _list;

        public abstract string Path { get; }

        public void Add(T model)
        {
            _list.Add(model);
        }
        

        public void AddRange(IEnumerable<T> models)
        {
            foreach (var item in models)
            {
                _list.Add(item);   
            }
        }

        public void Contains(T model)
        {
            _list.Contains(model);
        }

        public void Insert(int index, T model)
        {
            _list.Insert(index, model);
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(Path,true,Encoding.Default);
            foreach (var item in _list)
            {
                writer.WriteLine("{");
                WriteModel(writer, item);
                writer.WriteLine("}");
                count++;

            }
            _list.Clear();
            writer.Dispose();
            
            return count;
        }

        public IEnumerable<T> AsEnumerable()
        {
            StreamReader reader = File.OpenText(Path);
            T temp = null;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                switch (line)
                {
                    case "{":
                        temp = new T();
                        break;
                    case "}":
                        yield return temp;
                        break;
                    default:
                        ReadFill(line, temp);
                        break;
                }
            }
            reader.Dispose();
        }

        public abstract void WriteModel(StreamWriter writer, T models);
        public abstract void OnReadFill(string[] data, T model);

        public void ReadFill(string line,T model)
        {
            string[] data = line.Split(':');
            if (data.Length < 2)
                return;
            OnReadFill(data, model);
        }
    }
}
